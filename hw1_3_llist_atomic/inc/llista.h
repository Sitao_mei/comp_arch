//File Name: llista.h
//Author: Sitao Mei / 011817689
//Email Address: sitao.mei@gmail.com
//Description: list atomic.
//Last Changed: Sep 9,2017

#ifndef LLISTA_H_
#define LLISTA_H_

#include <string>
#include <atomic>
#include <thread>
#include <vector>
using namespace std;

struct Node {
	int data;
	Node* next;
};

class Llista
{
public:
	//Constructor and the destructors
	Llista();
	~Llista();

	//function insertion
	void append(int data);

	atomic<Node*> head;
	atomic<Node*> tail;
};

/*
class Node
{
	//Constructor and the destructors.
	Node();
	~Node();

	//getter and setter
	int data ;
	Node *next;
};
*/
#endif /* LLISTA_H_ */
