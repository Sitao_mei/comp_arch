	.cpu arm7tdmi
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"llista.cpp"
	.section	.rodata
	.align	2
	.type	_ZStL19piecewise_construct, %object
	.size	_ZStL19piecewise_construct, 1
_ZStL19piecewise_construct:
	.space	1
	.text
	.align	2
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZL20__gthread_key_deletei, %function
_ZL20__gthread_key_deletei:
	.fnstart
.LFB514:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #12
	str	r0, [fp, #-8]
	mov	r3, #0
	mov	r0, r3
	add	sp, fp, #0
	@ sp needed
	ldr	fp, [sp], #4
	bx	lr
	.cantunwind
	.fnend
	.size	_ZL20__gthread_key_deletei, .-_ZL20__gthread_key_deletei
	.section	.text._ZStanSt12memory_orderSt23__memory_order_modifier,"axG",%progbits,_ZStanSt12memory_orderSt23__memory_order_modifier,comdat
	.align	2
	.weak	_ZStanSt12memory_orderSt23__memory_order_modifier
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZStanSt12memory_orderSt23__memory_order_modifier, %function
_ZStanSt12memory_orderSt23__memory_order_modifier:
	.fnstart
.LFB858:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #12
	mov	r3, r0
	str	r1, [fp, #-12]
	strb	r3, [fp, #-5]
	ldr	r3, [fp, #-12]
	lsl	r3, r3, #24
	asr	r2, r3, #24
	ldrsb	r3, [fp, #-5]
	and	r3, r3, r2
	lsl	r3, r3, #24
	asr	r3, r3, #24
	and	r3, r3, #255
	mov	r0, r3
	add	sp, fp, #0
	@ sp needed
	ldr	fp, [sp], #4
	bx	lr
	.cantunwind
	.fnend
	.size	_ZStanSt12memory_orderSt23__memory_order_modifier, .-_ZStanSt12memory_orderSt23__memory_order_modifier
	.section	.text._ZSt24__cmpexch_failure_order2St12memory_order,"axG",%progbits,_ZSt24__cmpexch_failure_order2St12memory_order,comdat
	.align	2
	.weak	_ZSt24__cmpexch_failure_order2St12memory_order
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZSt24__cmpexch_failure_order2St12memory_order, %function
_ZSt24__cmpexch_failure_order2St12memory_order:
	.fnstart
.LFB859:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #12
	mov	r3, r0
	strb	r3, [fp, #-5]
	ldrb	r3, [fp, #-5]	@ zero_extendqisi2
	cmp	r3, #4
	beq	.L6
	ldrb	r3, [fp, #-5]	@ zero_extendqisi2
	cmp	r3, #3
	beq	.L7
	ldrb	r3, [fp, #-5]	@ zero_extendqisi2
	b	.L9
.L7:
	mov	r3, #0
	b	.L9
.L6:
	mov	r3, #2
.L9:
	mov	r0, r3
	add	sp, fp, #0
	@ sp needed
	ldr	fp, [sp], #4
	bx	lr
	.cantunwind
	.fnend
	.size	_ZSt24__cmpexch_failure_order2St12memory_order, .-_ZSt24__cmpexch_failure_order2St12memory_order
	.section	.text._ZSt23__cmpexch_failure_orderSt12memory_order,"axG",%progbits,_ZSt23__cmpexch_failure_orderSt12memory_order,comdat
	.align	2
	.weak	_ZSt23__cmpexch_failure_orderSt12memory_order
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZSt23__cmpexch_failure_orderSt12memory_order, %function
_ZSt23__cmpexch_failure_orderSt12memory_order:
	.fnstart
.LFB860:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, fp, lr}
	add	fp, sp, #8
	sub	sp, sp, #12
	mov	r3, r0
	strb	r3, [fp, #-13]
	ldrb	r3, [fp, #-13]	@ zero_extendqisi2
	ldr	r1, .L13
	mov	r0, r3
	bl	_ZStanSt12memory_orderSt23__memory_order_modifier
	mov	r3, r0
	mov	r0, r3
	bl	_ZSt24__cmpexch_failure_order2St12memory_order
	mov	r3, r0
	mov	r4, r3
	ldrb	r3, [fp, #-13]	@ zero_extendqisi2
	ldr	r1, .L13+4
	mov	r0, r3
	bl	_ZStanSt12memory_orderSt23__memory_order_modifier
	mov	r3, r0
	orr	r3, r4, r3
	and	r3, r3, #255
	mov	r0, r3
	sub	sp, fp, #8
	@ sp needed
	pop	{r4, fp, lr}
	bx	lr
.L14:
	.align	2
.L13:
	.word	65535
	.word	-65536
	.cantunwind
	.fnend
	.size	_ZSt23__cmpexch_failure_orderSt12memory_order, .-_ZSt23__cmpexch_failure_orderSt12memory_order
	.section	.rodata
	.align	2
	.type	_ZStL13allocator_arg, %object
	.size	_ZStL13allocator_arg, 1
_ZStL13allocator_arg:
	.space	1
	.align	2
	.type	_ZStL6ignore, %object
	.size	_ZStL6ignore, 1
_ZStL6ignore:
	.space	1
	.type	_ZN9__gnu_cxxL21__default_lock_policyE, %object
	.size	_ZN9__gnu_cxxL21__default_lock_policyE, 1
_ZN9__gnu_cxxL21__default_lock_policyE:
	.space	1
	.bss
	.align	2
_ZStL8__ioinit:
	.space	1
	.size	_ZStL8__ioinit, 1
	.section	.text._ZNSt13__atomic_baseIP4NodeEC2Ev,"axG",%progbits,_ZNSt13__atomic_baseIP4NodeEC5Ev,comdat
	.align	2
	.weak	_ZNSt13__atomic_baseIP4NodeEC2Ev
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZNSt13__atomic_baseIP4NodeEC2Ev, %function
_ZNSt13__atomic_baseIP4NodeEC2Ev:
	.fnstart
.LFB3073:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	@ link register save eliminated.
	str	fp, [sp, #-4]!
	add	fp, sp, #0
	sub	sp, sp, #12
	str	r0, [fp, #-8]
	ldr	r3, [fp, #-8]
	mov	r0, r3
	add	sp, fp, #0
	@ sp needed
	ldr	fp, [sp], #4
	bx	lr
	.cantunwind
	.fnend
	.size	_ZNSt13__atomic_baseIP4NodeEC2Ev, .-_ZNSt13__atomic_baseIP4NodeEC2Ev
	.weak	_ZNSt13__atomic_baseIP4NodeEC1Ev
	.set	_ZNSt13__atomic_baseIP4NodeEC1Ev,_ZNSt13__atomic_baseIP4NodeEC2Ev
	.section	.text._ZNSt6atomicIP4NodeEC2Ev,"axG",%progbits,_ZNSt6atomicIP4NodeEC5Ev,comdat
	.align	2
	.weak	_ZNSt6atomicIP4NodeEC2Ev
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZNSt6atomicIP4NodeEC2Ev, %function
_ZNSt6atomicIP4NodeEC2Ev:
	.fnstart
.LFB3075:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #8
	str	r0, [fp, #-8]
	ldr	r3, [fp, #-8]
	mov	r0, r3
	bl	_ZNSt13__atomic_baseIP4NodeEC1Ev
	ldr	r3, [fp, #-8]
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZNSt6atomicIP4NodeEC2Ev, .-_ZNSt6atomicIP4NodeEC2Ev
	.weak	_ZNSt6atomicIP4NodeEC1Ev
	.set	_ZNSt6atomicIP4NodeEC1Ev,_ZNSt6atomicIP4NodeEC2Ev
	.text
	.align	2
	.global	_ZN6LlistaC2Ev
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZN6LlistaC2Ev, %function
_ZN6LlistaC2Ev:
	.fnstart
.LFB3077:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #8
	str	r0, [fp, #-8]
	ldr	r3, [fp, #-8]
	mov	r0, r3
	bl	_ZNSt6atomicIP4NodeEC1Ev
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r0, r3
	bl	_ZNSt6atomicIP4NodeEC1Ev
	ldr	r3, [fp, #-8]
	mov	r1, #0
	mov	r0, r3
	bl	_ZNSt6atomicIP4NodeEaSES1_
	ldr	r3, [fp, #-8]
	add	r3, r3, #4
	mov	r1, #0
	mov	r0, r3
	bl	_ZNSt6atomicIP4NodeEaSES1_
	ldr	r3, [fp, #-8]
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN6LlistaC2Ev, .-_ZN6LlistaC2Ev
	.global	_ZN6LlistaC1Ev
	.set	_ZN6LlistaC1Ev,_ZN6LlistaC2Ev
	.align	2
	.global	_ZN6LlistaD2Ev
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZN6LlistaD2Ev, %function
_ZN6LlistaD2Ev:
	.fnstart
.LFB3080:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #16
	str	r0, [fp, #-16]
	ldr	r3, [fp, #-16]
	mov	r0, r3
	bl	_ZNKSt6atomicIP4NodeEcvS1_Ev
	str	r0, [fp, #-8]
.L23:
	ldr	r3, [fp, #-8]
	cmp	r3, #0
	beq	.L22
	ldr	r2, [fp, #-16]
	ldr	r3, [fp, #-8]
	ldr	r3, [r3, #4]
	mov	r1, r3
	mov	r0, r2
	bl	_ZNSt6atomicIP4NodeEaSES1_
	ldr	r0, [fp, #-8]
	bl	_ZdlPv
	ldr	r3, [fp, #-16]
	mov	r0, r3
	bl	_ZNKSt6atomicIP4NodeEcvS1_Ev
	str	r0, [fp, #-8]
	b	.L23
.L22:
	ldr	r3, [fp, #-16]
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN6LlistaD2Ev, .-_ZN6LlistaD2Ev
	.global	_ZN6LlistaD1Ev
	.set	_ZN6LlistaD1Ev,_ZN6LlistaD2Ev
	.align	2
	.global	_ZN6Llista6appendEi
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZN6Llista6appendEi, %function
_ZN6Llista6appendEi:
	.fnstart
.LFB3082:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	.save {fp, lr}
	.setfp fp, sp, #4
	add	fp, sp, #4
	.pad #16
	sub	sp, sp, #16
	str	r0, [fp, #-16]
	str	r1, [fp, #-20]
	mov	r0, #8
	bl	_Znwj
	mov	r3, r0
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-8]
	ldr	r2, [fp, #-20]
	str	r2, [r3]
	ldr	r3, [fp, #-8]
	mov	r2, #0
	str	r2, [r3, #4]
.L27:
	ldr	r3, [fp, #-16]
	add	r3, r3, #4
	mov	r0, r3
	bl	_ZNKSt6atomicIP4NodeEcvS1_Ev
	mov	r3, r0
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-16]
	add	r0, r3, #4
	sub	r1, fp, #12
	mov	r3, #5
	ldr	r2, [fp, #-8]
	bl	_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_order
	mov	r3, r0
	eor	r3, r3, #1
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L26
	b	.L27
.L26:
	ldr	r3, [fp, #-12]
	cmp	r3, #0
	beq	.L28
	ldr	r3, [fp, #-12]
	ldr	r2, [fp, #-8]
	str	r2, [r3, #4]
.L28:
	ldr	r3, [fp, #-16]
	mov	r0, r3
	bl	_ZNKSt6atomicIP4NodeEcvS1_Ev
	mov	r3, r0
	cmp	r3, #0
	moveq	r3, #1
	movne	r3, #0
	and	r3, r3, #255
	cmp	r3, #0
	beq	.L30
	ldr	r3, [fp, #-16]
	ldr	r1, [fp, #-8]
	mov	r0, r3
	bl	_ZNSt6atomicIP4NodeEaSES1_
.L30:
	nop
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
	.fnend
	.size	_ZN6Llista6appendEi, .-_ZN6Llista6appendEi
	.section	.text._ZNSt6atomicIP4NodeEaSES1_,"axG",%progbits,_ZNSt6atomicIP4NodeEaSES1_,comdat
	.align	2
	.weak	_ZNSt6atomicIP4NodeEaSES1_
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZNSt6atomicIP4NodeEaSES1_, %function
_ZNSt6atomicIP4NodeEaSES1_:
	.fnstart
.LFB3257:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #8
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	ldr	r3, [fp, #-8]
	ldr	r1, [fp, #-12]
	mov	r0, r3
	bl	_ZNSt13__atomic_baseIP4NodeEaSES1_
	mov	r3, r0
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZNSt6atomicIP4NodeEaSES1_, .-_ZNSt6atomicIP4NodeEaSES1_
	.section	.text._ZNKSt6atomicIP4NodeEcvS1_Ev,"axG",%progbits,_ZNKSt6atomicIP4NodeEcvS1_Ev,comdat
	.align	2
	.weak	_ZNKSt6atomicIP4NodeEcvS1_Ev
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZNKSt6atomicIP4NodeEcvS1_Ev, %function
_ZNKSt6atomicIP4NodeEcvS1_Ev:
	.fnstart
.LFB3258:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #8
	str	r0, [fp, #-8]
	ldr	r3, [fp, #-8]
	mov	r0, r3
	bl	_ZNKSt13__atomic_baseIP4NodeEcvS1_Ev
	mov	r3, r0
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZNKSt6atomicIP4NodeEcvS1_Ev, .-_ZNKSt6atomicIP4NodeEcvS1_Ev
	.section	.text._ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_order,"axG",%progbits,_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_order,comdat
	.align	2
	.weak	_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_order
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_order, %function
_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_order:
	.fnstart
.LFB3259:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #24
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	str	r2, [fp, #-16]
	strb	r3, [fp, #-17]
	ldrb	r3, [fp, #-17]	@ zero_extendqisi2
	mov	r0, r3
	bl	_ZSt23__cmpexch_failure_orderSt12memory_order
	mov	r3, r0
	mov	r2, r3
	ldrb	r3, [fp, #-17]	@ zero_extendqisi2
	str	r2, [sp]
	ldr	r2, [fp, #-16]
	ldr	r1, [fp, #-12]
	ldr	r0, [fp, #-8]
	bl	_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_orderS4_
	mov	r3, r0
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_order, .-_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_order
	.global	__sync_synchronize
	.section	.text._ZNSt13__atomic_baseIP4NodeEaSES1_,"axG",%progbits,_ZNSt13__atomic_baseIP4NodeEaSES1_,comdat
	.align	2
	.weak	_ZNSt13__atomic_baseIP4NodeEaSES1_
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZNSt13__atomic_baseIP4NodeEaSES1_, %function
_ZNSt13__atomic_baseIP4NodeEaSES1_:
	.fnstart
.LFB3319:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, r5, fp, lr}
	add	fp, sp, #12
	sub	sp, sp, #24
	str	r0, [fp, #-32]
	str	r1, [fp, #-36]
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-16]
	ldr	r3, [fp, #-36]
	str	r3, [fp, #-20]
	mov	r3, #5
	strb	r3, [fp, #-21]
	ldrb	r3, [fp, #-21]	@ zero_extendqisi2
	ldr	r1, .L39
	mov	r0, r3
	bl	_ZStanSt12memory_orderSt23__memory_order_modifier
	mov	r3, r0
	strb	r3, [fp, #-22]
	ldr	r4, [fp, #-16]
	ldr	r5, [fp, #-20]
	bl	__sync_synchronize
	str	r5, [r4]
	bl	__sync_synchronize
	ldr	r3, [fp, #-36]
	mov	r0, r3
	sub	sp, fp, #12
	@ sp needed
	pop	{r4, r5, fp, lr}
	bx	lr
.L40:
	.align	2
.L39:
	.word	65535
	.cantunwind
	.fnend
	.size	_ZNSt13__atomic_baseIP4NodeEaSES1_, .-_ZNSt13__atomic_baseIP4NodeEaSES1_
	.section	.text._ZNKSt13__atomic_baseIP4NodeEcvS1_Ev,"axG",%progbits,_ZNKSt13__atomic_baseIP4NodeEcvS1_Ev,comdat
	.align	2
	.weak	_ZNKSt13__atomic_baseIP4NodeEcvS1_Ev
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZNKSt13__atomic_baseIP4NodeEcvS1_Ev, %function
_ZNKSt13__atomic_baseIP4NodeEcvS1_Ev:
	.fnstart
.LFB3320:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r4, fp, lr}
	add	fp, sp, #8
	sub	sp, sp, #20
	str	r0, [fp, #-24]
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-16]
	mov	r3, #5
	strb	r3, [fp, #-17]
	ldrb	r3, [fp, #-17]	@ zero_extendqisi2
	ldr	r1, .L44
	mov	r0, r3
	bl	_ZStanSt12memory_orderSt23__memory_order_modifier
	mov	r3, r0
	strb	r3, [fp, #-18]
	ldr	r4, [fp, #-16]
	bl	__sync_synchronize
	ldr	r4, [r4]
	bl	__sync_synchronize
	mov	r3, r4
	nop
	mov	r0, r3
	sub	sp, fp, #8
	@ sp needed
	pop	{r4, fp, lr}
	bx	lr
.L45:
	.align	2
.L44:
	.word	65535
	.cantunwind
	.fnend
	.size	_ZNKSt13__atomic_baseIP4NodeEcvS1_Ev, .-_ZNKSt13__atomic_baseIP4NodeEcvS1_Ev
	.section	.text._ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_orderS4_,"axG",%progbits,_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_orderS4_,comdat
	.align	2
	.weak	_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_orderS4_
	.syntax unified
	.arm
	.fpu softvfp
	.type	_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_orderS4_, %function
_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_orderS4_:
	.fnstart
.LFB3321:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #40
	str	r0, [fp, #-24]
	str	r1, [fp, #-28]
	str	r2, [fp, #-32]
	strb	r3, [fp, #-33]
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-32]
	str	r3, [fp, #-16]
	ldrb	r3, [fp, #-33]
	strb	r3, [fp, #-17]
	ldrb	r3, [fp, #4]
	strb	r3, [fp, #-18]
	ldrb	r3, [fp, #-18]	@ zero_extendqisi2
	ldr	r1, .L49
	mov	r0, r3
	bl	_ZStanSt12memory_orderSt23__memory_order_modifier
	mov	r3, r0
	strb	r3, [fp, #-19]
	ldrb	r3, [fp, #-17]	@ zero_extendqisi2
	ldr	r1, .L49
	mov	r0, r3
	bl	_ZStanSt12memory_orderSt23__memory_order_modifier
	mov	r3, r0
	strb	r3, [fp, #-20]
	ldr	r0, [fp, #-8]
	ldr	r2, [fp, #-16]
	ldrb	r1, [fp, #-17]	@ zero_extendqisi2
	ldrb	r3, [fp, #-18]	@ zero_extendqisi2
	str	r3, [sp]
	mov	r3, r1
	ldr	r1, [fp, #-12]
	bl	__atomic_compare_exchange_4
	mov	r3, r0
	mov	r0, r3
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
.L50:
	.align	2
.L49:
	.word	65535
	.cantunwind
	.fnend
	.size	_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_orderS4_, .-_ZNSt6atomicIP4NodeE21compare_exchange_weakERS1_S1_St12memory_orderS4_
	.text
	.align	2
	.syntax unified
	.arm
	.fpu softvfp
	.type	_Z41__static_initialization_and_destruction_0ii, %function
_Z41__static_initialization_and_destruction_0ii:
	.fnstart
.LFB3405:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	sub	sp, sp, #8
	str	r0, [fp, #-8]
	str	r1, [fp, #-12]
	ldr	r3, [fp, #-8]
	cmp	r3, #1
	bne	.L53
	ldr	r3, [fp, #-12]
	ldr	r2, .L54
	cmp	r3, r2
	bne	.L53
	ldr	r0, .L54+4
	bl	_ZNSt8ios_base4InitC1Ev
	ldr	r2, .L54+8
	ldr	r1, .L54+12
	ldr	r0, .L54+4
	bl	__aeabi_atexit
.L53:
	nop
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
.L55:
	.align	2
.L54:
	.word	65535
	.word	_ZStL8__ioinit
	.word	__dso_handle
	.word	_ZNSt8ios_base4InitD1Ev
	.cantunwind
	.fnend
	.size	_Z41__static_initialization_and_destruction_0ii, .-_Z41__static_initialization_and_destruction_0ii
	.align	2
	.syntax unified
	.arm
	.fpu softvfp
	.type	_GLOBAL__sub_I__ZN6LlistaC2Ev, %function
_GLOBAL__sub_I__ZN6LlistaC2Ev:
	.fnstart
.LFB3406:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{fp, lr}
	add	fp, sp, #4
	ldr	r1, .L57
	mov	r0, #1
	bl	_Z41__static_initialization_and_destruction_0ii
	sub	sp, fp, #4
	@ sp needed
	pop	{fp, lr}
	bx	lr
.L58:
	.align	2
.L57:
	.word	65535
	.cantunwind
	.fnend
	.size	_GLOBAL__sub_I__ZN6LlistaC2Ev, .-_GLOBAL__sub_I__ZN6LlistaC2Ev
	.section	.init_array,"aw",%init_array
	.align	2
	.word	_GLOBAL__sub_I__ZN6LlistaC2Ev(target1)
	.hidden	__dso_handle
	.ident	"GCC: (GNU Tools for ARM Embedded Processors 6-2017-q2-update) 6.3.1 20170620 (release) [ARM/embedded-6-branch revision 249437]"
