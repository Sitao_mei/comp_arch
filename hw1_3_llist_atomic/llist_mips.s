	.file	1 "llista.cpp"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=xx
	.module	nooddspreg
	.text
	.align	2
	.globl	_ZN6LlistaC2Ev
.LFB2865 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN6LlistaC2Ev
	.type	_ZN6LlistaC2Ev, @function
_ZN6LlistaC2Ev:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	sync
	sw	$0,0($4)
	sync
	sync
	sw	$0,4($4)
	sync
	jr	$31
	nop

	.set	macro
	.set	reorder
	.end	_ZN6LlistaC2Ev
	.cfi_endproc
.LFE2865:
	.size	_ZN6LlistaC2Ev, .-_ZN6LlistaC2Ev
	.globl	_ZN6LlistaC1Ev
	_ZN6LlistaC1Ev = _ZN6LlistaC2Ev
	.align	2
	.globl	_ZN6LlistaD2Ev
.LFB2868 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN6LlistaD2Ev
	.type	_ZN6LlistaD2Ev, @function
_ZN6LlistaD2Ev:
	.frame	$sp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	.cfi_def_cfa_offset 24
	sw	$16,16($sp)
	.cfi_offset 16, -8
	move	$16,$4
	sw	$31,20($sp)
	.cfi_offset 31, -4
	sync
	lw	$2,0($4)
	sync
	beq	$2,$0,.L2
	move	$4,$2

.L6:
	sync
	lw	$2,4($4)
	sw	$2,0($16)
	sync
	jal	_ZdlPv
	nop

	sync
	lw	$4,0($16)
	sync
	bne	$4,$0,.L6
	nop

.L2:
	lw	$31,20($sp)
	lw	$16,16($sp)
	jr	$31
	addiu	$sp,$sp,24

	.cfi_def_cfa_offset 0
	.cfi_restore 16
	.cfi_restore 31
	.set	macro
	.set	reorder
	.end	_ZN6LlistaD2Ev
	.cfi_endproc
.LFE2868:
	.size	_ZN6LlistaD2Ev, .-_ZN6LlistaD2Ev
	.globl	_ZN6LlistaD1Ev
	_ZN6LlistaD1Ev = _ZN6LlistaD2Ev
	.align	2
	.globl	_ZN6Llista6appendEi
.LFB2870 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_ZN6Llista6appendEi
	.type	_ZN6Llista6appendEi, @function
_ZN6Llista6appendEi:
	.frame	$sp,40,$31		# vars= 8, regs= 3/0, args= 16, gp= 0
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	.cfi_def_cfa_offset 40
	sw	$16,28($sp)
	.cfi_offset 16, -12
	move	$16,$4
	li	$4,8			# 0x8
	sw	$17,32($sp)
	sw	$31,36($sp)
	.cfi_offset 17, -8
	.cfi_offset 31, -4
	jal	_Znwm
	move	$17,$5

	addiu	$3,$16,4
	sw	$17,0($2)
	sw	$0,4($2)
.L12:
	sync
	lw	$5,0($3) //Notes : Load "tail($3)" into $5
	sync
	sw	$5,16($sp) // Notes: Then store "tail" in $5 into $sp+16 
	.set	noat
	sync
1:
	ll	$7,0($3)  // Notes: Load lock "tail"($3) into reg $7, Acquire Lock.
	bne	$7,$5,2f  // Loop to pc_2f if $7(new load "tail") != record "tail" in $5.  otherwise , continue.
	li	$6,0  
	move	$1,$2
	sc	$1,0($3) //"tail($3)"= "new node($1)" , Release lock.
	beq	$1,$0,1b
	li	$6,1
	sync
2:
	.set	at
	beq	$6,$0,.L23
	nop

	lw	$3,16($sp)
	beq	$3,$0,.L13
	nop

	sw	$2,4($3)
.L13:
	sync
	lw	$3,0($16)
	sync
	beq	$3,$0,.L24
	lw	$31,36($sp)

	lw	$17,32($sp)
	lw	$16,28($sp)
	jr	$31
	addiu	$sp,$sp,40

	.cfi_remember_state
	.cfi_def_cfa_offset 0
	.cfi_restore 16
	.cfi_restore 17
	.cfi_restore 31
.L23:
	.cfi_restore_state
	b	.L12
	sw	$7,16($sp)

.L24:
	sync
	sw	$2,0($16)
	sync
	lw	$31,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	jr	$31
	addiu	$sp,$sp,40

	.cfi_def_cfa_offset 0
	.cfi_restore 31
	.cfi_restore 17
	.cfi_restore 16
	.set	macro
	.set	reorder
	.end	_ZN6Llista6appendEi
	.cfi_endproc
.LFE2870:
	.size	_ZN6Llista6appendEi, .-_ZN6Llista6appendEi
	.section	.text.startup,"ax",@progbits
	.align	2
.LFB2935 = .
	.cfi_startproc
	.set	nomips16
	.set	nomicromips
	.ent	_GLOBAL__sub_I__ZN6LlistaC2Ev
	.type	_GLOBAL__sub_I__ZN6LlistaC2Ev, @function
_GLOBAL__sub_I__ZN6LlistaC2Ev:
	.frame	$sp,24,$31		# vars= 0, regs= 1/0, args= 16, gp= 0
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	.cfi_def_cfa_offset 24
	sw	$31,20($sp)
	.cfi_offset 31, -4
	jal	_ZNSt8ios_base4InitC1Ev
	addiu	$4,$28,%gp_rel(_ZStL8__ioinit)

	lui	$4,%hi(_ZNSt8ios_base4InitD1Ev)
	lw	$31,20($sp)
	addiu	$5,$28,%gp_rel(_ZStL8__ioinit)
	addiu	$4,$4,%lo(_ZNSt8ios_base4InitD1Ev)
	addiu	$6,$28,%gp_rel(__dso_handle)
	.cfi_restore 31
	j	__cxa_atexit
	addiu	$sp,$sp,24

	.cfi_def_cfa_offset 0
	.set	macro
	.set	reorder
	.end	_GLOBAL__sub_I__ZN6LlistaC2Ev
	.cfi_endproc
.LFE2935:
	.size	_GLOBAL__sub_I__ZN6LlistaC2Ev, .-_GLOBAL__sub_I__ZN6LlistaC2Ev
	.section	.ctors,"aw",@progbits
	.align	2
	.word	_GLOBAL__sub_I__ZN6LlistaC2Ev
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,4
	.hidden	__dso_handle
	.ident	"GCC: (Codescape GNU Tools 2016.05-03 for MIPS MTI Bare Metal) 4.9.2"
