//File Name: llista.h
//Author: Sitao Mei / 011817689
//Email Address: sitao.mei@gmail.com
//Description: list atomic.
//Last Changed: Sep 9,2017

#include "llista.h"
#include <atomic>
#include <thread>
#include <vector>
#include <iostream>

//Constructor 
Llista::Llista()
{
	head=nullptr;
	tail=nullptr;
}

//Destructor
Llista::~Llista()
{
	Node *node ;
	node = head ;
	// delete all node from list.
	while (node!= NULL)
	{
		head = node->next ;
		delete node;
		node = head;
	}
}

// List append from tail 
void Llista::append(int data)
{
	Node* new_node= new Node;

	new_node->data=data;
	new_node->next=nullptr;
	Node* old_tail;

	// atomic update tail pointer
	do
	{
		old_tail=tail;

	} while ( !tail.compare_exchange_weak(old_tail,new_node) );

	// Then update new tail into old tail's next ptr.
	if (old_tail!=nullptr) 
	{
		old_tail->next = new_node;
	}

	// Check to update head pointer for null list
	if (head==nullptr)
	{
		head=new_node;
	}
}



