	.file	"llista.cpp"
	.intel_syntax noprefix
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB0:
	.text
.LHOTB0:
	.align 2
	.p2align 4,,15
	.globl	_ZN6LlistaC2Ev
	.type	_ZN6LlistaC2Ev, @function
_ZN6LlistaC2Ev:
.LFB3189:
	.cfi_startproc
	xor	eax, eax
	mov	QWORD PTR [rdi], rax
	mfence
	mov	QWORD PTR [rdi+8], rax
	mfence
	ret
	.cfi_endproc
.LFE3189:
	.size	_ZN6LlistaC2Ev, .-_ZN6LlistaC2Ev
	.section	.text.unlikely
.LCOLDE0:
	.text
.LHOTE0:
	.globl	_ZN6LlistaC1Ev
	.set	_ZN6LlistaC1Ev,_ZN6LlistaC2Ev
	.section	.text.unlikely
	.align 2
.LCOLDB1:
	.text
.LHOTB1:
	.align 2
	.p2align 4,,15
	.globl	_ZN6LlistaD2Ev
	.type	_ZN6LlistaD2Ev, @function
_ZN6LlistaD2Ev:
.LFB3192:
	.cfi_startproc
	push	rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	mov	rbx, rdi
	mov	rdi, QWORD PTR [rdi]
	test	rdi, rdi
	je	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	mov	rax, QWORD PTR [rdi+8]
	mov	QWORD PTR [rbx], rax
	mfence
	call	_ZdlPv
	mov	rdi, QWORD PTR [rbx]
	test	rdi, rdi
	jne	.L8
.L2:
	pop	rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE3192:
	.size	_ZN6LlistaD2Ev, .-_ZN6LlistaD2Ev
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.globl	_ZN6LlistaD1Ev
	.set	_ZN6LlistaD1Ev,_ZN6LlistaD2Ev
	.section	.text.unlikely
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4,,15
	.globl	_ZN6Llista6appendEi
	.type	_ZN6Llista6appendEi, @function
_ZN6Llista6appendEi:
.LFB3194:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	push	rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	mov	rbx, rdi
	mov	edi, 16
	mov	ebp, esi
	sub	rsp, 24
	.cfi_def_cfa_offset 48
	mov	rax, QWORD PTR fs:40
	mov	QWORD PTR [rsp+8], rax
	xor	eax, eax
	call	_Znwm
	lea	rdx, [rbx+8]
	mov	rcx, rax
	mov	DWORD PTR [rax], ebp
	mov	QWORD PTR [rax+8], 0
	.p2align 4,,10
	.p2align 3
.L14:
	mov	rax, QWORD PTR [rdx]
	mov	QWORD PTR [rsp], rax
	lock cmpxchg	QWORD PTR [rdx], rcx
	jne	.L23
	mov	rax, QWORD PTR [rsp]
	test	rax, rax
	je	.L15
	mov	QWORD PTR [rax+8], rcx
.L15:
	mov	rax, QWORD PTR [rbx]
	test	rax, rax
	je	.L24
.L12:
	mov	rax, QWORD PTR [rsp+8]
	xor	rax, QWORD PTR fs:40
	jne	.L25
	add	rsp, 24
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	pop	rbx
	.cfi_def_cfa_offset 16
	pop	rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	mov	QWORD PTR [rsp], rax
	jmp	.L14
.L25:
	call	__stack_chk_fail
.L24:
	mov	QWORD PTR [rbx], rcx
	mfence
	jmp	.L12
	.cfi_endproc
.LFE3194:
	.size	_ZN6Llista6appendEi, .-_ZN6Llista6appendEi
	.section	.text.unlikely
.LCOLDE2:
	.text
.LHOTE2:
	.section	.text.unlikely
.LCOLDB3:
	.section	.text.startup,"ax",@progbits
.LHOTB3:
	.p2align 4,,15
	.type	_GLOBAL__sub_I__ZN6LlistaC2Ev, @function
_GLOBAL__sub_I__ZN6LlistaC2Ev:
.LFB3441:
	.cfi_startproc
	sub	rsp, 8
	.cfi_def_cfa_offset 16
	mov	edi, OFFSET FLAT:_ZStL8__ioinit
	call	_ZNSt8ios_base4InitC1Ev
	mov	edx, OFFSET FLAT:__dso_handle
	mov	esi, OFFSET FLAT:_ZStL8__ioinit
	mov	edi, OFFSET FLAT:_ZNSt8ios_base4InitD1Ev
	add	rsp, 8
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE3441:
	.size	_GLOBAL__sub_I__ZN6LlistaC2Ev, .-_GLOBAL__sub_I__ZN6LlistaC2Ev
	.section	.text.unlikely
.LCOLDE3:
	.section	.text.startup
.LHOTE3:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN6LlistaC2Ev
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
