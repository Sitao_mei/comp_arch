//File Name: llista.h
//Author: Sitao Mei / 011817689
//Email Address: sitao.mei@gmail.com
//Description: list atomic.
//Last Changed: Sep 9,2017

#include "llista.h"
#include <thread>
#include <vector>
#include <iostream>

int main ()
{
  Llista list;
  // spawn 10 threads to fill the linked list:
  vector<thread> threads;

  for (int i=0; i<10; ++i) threads.push_back(thread(&Llista::append,&list,i));
  for (auto& th : threads) th.join();

  //for (int i=1; i<10; ++i) list.append(i);

  // print contents:
  for (Node* it = list.head; it!=nullptr; it=it->next)
  {
    cout << ' ' << it->data;
    cout << ' ' << it->next;
    cout << '\n';
  }

  // cleanup:
  Node* it; while (it=list.head) {list.head=it->next; delete it;}

  return 0;
}
